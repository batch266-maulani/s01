package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you? ");
        double age = new Double(userInput.nextLine());

        System.out.println("The age is " + age);
    }
}
